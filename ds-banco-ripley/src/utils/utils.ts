export function format(first: string, middle: string, last: string): string {
  return (first || '') + (middle ? ` ${middle}` : '') + (last ? ` ${last}` : '');
}

export function createControls(properties){
  //console.log(properties)
  let json = {};
  for(let key in properties){
      //get data Component
      let typeProperties = properties[key].type;
      //Transform type
      let typePropertiesTransform = typeProperties;
      if(typePropertiesTransform == 'string'){ typePropertiesTransform = 'text'; }
      //default Values
      let DVProperties   = properties[key].defaultValue;
      DVProperties       = DVProperties.replace(/['"]+/g, '');
      //Transform default Values
      if(DVProperties == 'true'){DVProperties = true;}
      if(DVProperties == 'false'){DVProperties = false;}
      //data
      let CTProperties   = properties[key].complexType.resolved;
      CTProperties       = CTProperties.replace(/['"]+/g, '');
      let CTBool         = false;
      //Utiliza Options
      if(CTProperties.includes('|')){ CTBool = true; }
      //Docs
      let descProperties = properties[key].docs.text;
      //create Json Controls
      json[key] = {
          options: CTBool,
          defaultValue: DVProperties,
          control: {
              type: typePropertiesTransform
          },
          description: descProperties,
          table: {
            type: { 
              summary: 'type: '+typeProperties 
            },
            defaultValue: { 
              summary: DVProperties 
            },
          }
      };
      if(CTBool == true){ //Agregamos Options
        const arrayOptions = CTProperties.split("|").map(function(item) {
          return item.trim();
        });
        json[key]["options"] = arrayOptions;
        json[key].control.type = 'select';
      }
  }
  return json;
}

export function createPreviewsCode(jsonObj){
  let array = [];
  if(isJson(jsonObj) == true){
    for(let key in jsonObj){
      if(key == 'css'){ //CSS
        const data = jsonObj[key];
        const pushData = {tab: 'CSS', template: data, language:'css', copy: true};
        array.push(pushData);
      }else if(key == 'html'){ //HTML
        const data = jsonObj[key];
        const pushData = {tab: 'HTML', template: data, language:'html', copy: true};
        array.push(pushData);
      }else if(key == 'js'){ //JS
        const data = jsonObj[key];
        const pushData = {tab: 'JavaScript', template: data, language:'ts', copy: true};
        array.push(pushData);
      }else{
        console.error("Error execute <createPreviewsCode>, param <"+key+"> undefined");
        return array;
      }
    }
    return array;
  }else{
    console.error("Error execute <createPreviewsCode>, you must send a json");
    return array;
  }
}

function isJson(item) {
  item = typeof item !== "string"
      ? JSON.stringify(item)
      : item;

  try {
      item = JSON.parse(item);
  } catch (e) {
      return false;
  }

  if (typeof item === "object" && item !== null) {
      return true;
  }

  return false;
}

import React from 'react';
//Readme
import readme from './readme.md';
//Resources Component
import { SwitchComponent }  from '../../../dist/collection/components/switch-component/switch-component';
import cssCode              from '!!raw-loader!../../../dist/collection/components/switch-component/switch-component.css';
//Utils Functions
import { createControls, createPreviewsCode } from '../../utils/utils'
const controls    = createControls(SwitchComponent.properties);
const previewData = createPreviewsCode({css: cssCode});

export default {
  title: 'Design System/Atoms/Switch',
  component: SwitchComponent,
  argTypes: controls,
  parameters: {
    notes: readme,
    preview: previewData
  }
};

const defaultArgs = {
  disabled: false,
};

const Template = args => {
  return <switch-component {...args}></switch-component>;
};

export const Default = Template.bind({});
Default.args = { ...defaultArgs };
# switch-component



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description  | Type                          | Default    |
| -------- | --------- | ------------ | ----------------------------- | ---------- |
| `helper` | `helper`  | label switch | `string`                      | `'helper'` |
| `label`  | `label`   | label switch | `string`                      | `'Label'`  |
| `state`  | `state`   | Color Icon   | `"disabled" \| "off" \| "on"` | `'on'`     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

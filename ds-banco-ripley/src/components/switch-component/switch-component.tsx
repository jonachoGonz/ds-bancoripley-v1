import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'switch-component',
  styleUrl: 'switch-component.scss',
  shadow: true,
})
export class SwitchComponent {
  /**
  * Color Icon
  * 
  */
  @Prop() state:  'on' | 'off' | 'disabled' = 'on';
  /**
  * label switch
  * 
  */
  @Prop() label:  string = 'Label';
  /**
  * label switch
  * 
  */
  @Prop() helper:  string = 'helper';

  render() {
    return (
      <div class={'inputGroup inputGroup-content inputGroup-'+this.state}>
        <label>
          {this.state == 'disabled'
              ?<input type="checkbox" name="checkbox-toggle" value="checkbox-toggle" aria-describedby="checkbox-toggle" disabled/>
              :<input type="checkbox" name="checkbox-toggle" value="checkbox-toggle" aria-describedby="checkbox-toggle" />
            }
          <span id="checkbox-toggle" class="inputGroup-content-switch">
            <span class="inputGroup-switch"></span>
            {/* {this.state == 'disabled'
              ? <span class="inputGroup-helper">disabled</span>
              : <span class="inputGroup-helper">active</span>
            } */}
          </span>
          <span class="inputGroup-label">{this.label}</span>
        </label>
      </div>
    );
  }
}

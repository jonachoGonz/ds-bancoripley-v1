import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'input-atoms',
  styleUrl: 'inputs-atoms.scss',
  shadow: true,
})
export class InputsAtoms {
  /**
   * Input label
   * 
   */
  @Prop() label: string = 'Label';
  /**
   * Input placeholder
   * 
   */
  @Prop() placeholder: string = 'Placeholder';
  /**
   * Input assistive
   * 
   */
  @Prop() helper: string = 'Helper Text';
  /**
   * Input type
   * 
   */
  @Prop() type: 'text'|'phone'|'password'|'search'|'textArea'|'date'|'select' = 'text';
  /**
   * Input state
   * 
   */
  @Prop() state: 'error'|'focus'|'disabled'|'active'|'default' = 'default';
 
  render() {
      if (this.type == 'textArea') {
        return ( 
          <inputs-atoms>
            <div class={'inputGroup inputGroup-content  inputGroup-'+this.state}>
              <label class="inputGroup-label">{this.label}</label>
              <div class={' inputGroup-'+this.type}>
                <textarea placeholder={this.placeholder} ></textarea>
                {this.state == 'error'
                  ? <icon-component size="24" icon="error" color="black"></icon-component>
                  : ''
                }

              </div>
              <span class="inputGroup-helper">{this.helper}</span>
            </div>
          </inputs-atoms>
        )
      } else if (this.type == 'select') {
        return (
          <inputs-atoms>
            <div class={'inputGroup inputGroup-content  inputGroup-'+this.state}>
              <label class="inputGroup-label">{this.label}</label>
              <div class={'inputGroup-'+this.type}>
                <select name="select" id="">
                  <option value={this.placeholder} selected>{this.placeholder}</option>
                  <option value="option 1">option 1</option>
                  <option value="option 2">option 2</option>
                </select>
              </div>
              
              <span class="inputGroup-helper">{this.helper}</span>
            </div>
          </inputs-atoms>
        )
      } else if (this.type == 'search') {
        return (
          <inputs-atoms>
            <div class={'inputGroup inputGroup-content  inputGroup-'+this.state}>
              <label class="inputGroup-label">{this.label}</label>
              <div class={'inputGroup-'+this.type}>
                <input type={this.type} placeholder={this.placeholder} />
                {/* <button class='inputGroup-btnCancel'>
                  <icon-component size="16" icon="close" color="black"></icon-component>
                </button> */}
                <button class='inputGroup-btnSearch'>
                  <icon-component size="24" icon="search" color="black"></icon-component>
                </button>
              </div>
              
              <span class="inputGroup-helper">{this.helper}</span>
            </div>
          </inputs-atoms>
        )
      } else {
        return (
          <inputs-atoms>
            <div class={'inputGroup inputGroup-content inputGroup-'+this.state}>
              <label class="inputGroup-label">{this.label}</label>
              <div class={'inputGroup-'+this.type}>
                <input type={this.type} placeholder={this.placeholder} />
                {this.type == 'password'
                  ? <icon-component size="24" icon="visibility" color="black"></icon-component>
                  : ''
                }
                {this.state == 'error'
                  ? <icon-component size="24" icon="error" color="black"></icon-component>
                  : ''
                }
              </div>
              <span class="inputGroup-helper">{this.helper}</span>
            </div>
          </inputs-atoms>
        )
      }
  }
}

import React from 'react';
//Readme
import readme from './readme.md';
//Resources Component
import { InputsAtoms }  from '../../../dist/collection/components/inputs-atoms/inputs-atoms';
import cssCode              from '!!raw-loader!../../../dist/collection/components/inputs-atoms/inputs-atoms.css';
//Utils Functions
import { createControls, createPreviewsCode } from '../../utils/utils'
const controls    = createControls(InputsAtoms.properties);
const previewData = createPreviewsCode({css: cssCode});

export default {
  title: 'Design System/Atoms/Inputs',
  component: InputsAtoms,
  argTypes: controls,
  parameters: {
    notes: readme,
    preview: previewData
  }
};

const defaultArgs = {
  disabled: false,
};

const Template = args => {
  return <input-atoms {...args}></input-atoms>;
};

export const Default = Template.bind({});
Default.args = { ...defaultArgs };
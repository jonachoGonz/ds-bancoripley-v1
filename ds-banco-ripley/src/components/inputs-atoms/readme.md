# inputs-atoms



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description       | Type                                                                              | Default         |
| ------------- | ------------- | ----------------- | --------------------------------------------------------------------------------- | --------------- |
| `helper`      | `helper`      | Input assistive   | `string`                                                                          | `'Helper Text'` |
| `label`       | `label`       | Input label       | `string`                                                                          | `'Label'`       |
| `placeholder` | `placeholder` | Input placeholder | `string`                                                                          | `'Placeholder'` |
| `state`       | `state`       | Input state       | `"active" \| "default" \| "disabled" \| "error" \| "focus"`                       | `'default'`     |
| `type`        | `type`        | Input type        | `"date" \| "password" \| "phone" \| "search" \| "select" \| "text" \| "textArea"` | `'text'`        |


## Dependencies

### Used by

 - [login-component](../login-component)

### Depends on

- [icon-component](../icon-component)

### Graph
```mermaid
graph TD;
  input-atoms --> icon-component
  login-component --> input-atoms
  style input-atoms fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

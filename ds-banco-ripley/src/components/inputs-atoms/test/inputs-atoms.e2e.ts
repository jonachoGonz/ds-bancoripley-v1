import { newE2EPage } from '@stencil/core/testing';

describe('inputs-atoms', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<inputs-atoms></inputs-atoms>');

    const element = await page.find('inputs-atoms');
    expect(element).toHaveClass('hydrated');
  });
});

import { newSpecPage } from '@stencil/core/testing';
import { InputsAtoms } from '../inputs-atoms';

describe('inputs-atoms', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [InputsAtoms],
      html: `<inputs-atoms></inputs-atoms>`,
    });
    expect(page.root).toEqualHtml(`
      <inputs-atoms>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </inputs-atoms>
    `);
  });
});

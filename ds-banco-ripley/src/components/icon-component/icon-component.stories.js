import React from 'react';
//Readme
import readme from './readme.md';
//Resources Component
import { IconComponent }  from '../../../dist/collection/components/icon-component/icon-component';
import cssCode              from '!!raw-loader!../../../dist/collection/components/icon-component/icon-component.css';
//Utils Functions
import { createControls, createPreviewsCode } from '../../utils/utils'
const controls    = createControls(IconComponent.properties);
const previewData = createPreviewsCode({css: cssCode});

export default {
  title: 'Design System/Atoms/Icons',
  component: IconComponent,
  argTypes: controls,
  parameters: {
    notes: readme,
    preview: previewData
  }
};

const defaultArgs = {
  disabled: false,
};

const Template = args => {
  return <icon-component {...args}></icon-component>;
};

export const Default = Template.bind({});
Default.args = { ...defaultArgs };
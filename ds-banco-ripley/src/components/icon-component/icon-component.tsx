import { Component, h , Prop} from '@stencil/core';

@Component({
  tag: 'icon-component',
  styleUrl: 'icon-component.scss',
  shadow: true
})
export class IconComponent {
  /**
   * Size Icon
   * 
   */
  @Prop() size: '16' | '24' | '40' = '16';
  /**
   * Name Icon
   * 
   */
  @Prop() icon: string = 'add';
  
  /**
   * Color Icon
   * 
   */
  @Prop() color: 'white' | 'primary' | 'secondary' | 'black' = 'primary';

  render() {
    return (
      <span class={'material-icons '+'icon-size-'+this.size +' icon-color-'+this.color}>{this.icon}</span>
    );
  }

}

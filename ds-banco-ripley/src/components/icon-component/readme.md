# icon-component



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type                                             | Default     |
| -------- | --------- | ----------- | ------------------------------------------------ | ----------- |
| `color`  | `color`   | Color Icon  | `"black" \| "primary" \| "secondary" \| "white"` | `'primary'` |
| `icon`   | `icon`    | Name Icon   | `string`                                         | `'add'`     |
| `size`   | `size`    | Size Icon   | `"16" \| "24" \| "40"`                           | `'16'`      |


## Dependencies

### Used by

 - [button-component](../button-component)
 - [input-atoms](../inputs-atoms)

### Graph
```mermaid
graph TD;
  button-component --> icon-component
  input-atoms --> icon-component
  style icon-component fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

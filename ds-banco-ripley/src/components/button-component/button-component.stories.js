import React from 'react';
//Readme
import readme from './readme.md';
//Resources Component
import { ButtonComponent }  from '../../../dist/collection/components/button-component/button-component';
import cssCode              from '!!raw-loader!../../../dist/collection/components/button-component/button-component.css';
//Utils Functions
import { createControls, createPreviewsCode } from '../../utils/utils'
const controls    = createControls(ButtonComponent.properties);
const previewData = createPreviewsCode({css: cssCode});

export default {
  title: 'Design System/Atoms/Button',
  component: ButtonComponent,
  argTypes: controls,
  parameters: {
    notes: readme,
    preview: previewData
  }
};

const defaultArgs = {
  disabled: false,
};

const Template = args => {
  return <button-component {...args}></button-component>;
};

export const Default = Template.bind({});
Default.args = { ...defaultArgs };
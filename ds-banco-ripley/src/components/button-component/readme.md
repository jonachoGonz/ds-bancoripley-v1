# button-component



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description  | Type                                                 | Default         |
| ---------- | ---------- | ------------ | ---------------------------------------------------- | --------------- |
| `disabled` | `disabled` | Disabled     | `boolean`                                            | `false`         |
| `icon`     | `icon`     | Icon Button  | `boolean`                                            | `false`         |
| `label`    | `label`    | Texto Button | `string`                                             | `'button'`      |
| `size`     | `size`     | Size Button  | `"btn-lg" \| "btn-md" \| "btn-sm"`                   | `'btn-lg'`      |
| `type`     | `type`     | Type Button  | `"btn-primary" \| "btn-secondary" \| "btn-tertiary"` | `'btn-primary'` |


## Dependencies

### Used by

 - [login-component](../login-component)

### Depends on

- [icon-component](../icon-component)

### Graph
```mermaid
graph TD;
  button-component --> icon-component
  login-component --> button-component
  style button-component fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

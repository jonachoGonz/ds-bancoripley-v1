import { Component, Prop, h } from '@stencil/core';
@Component({
  tag: 'button-component',
  styleUrl: 'button-component.scss',
  shadow: true,
})
export class ButtonComponent {
  /**
   * Type Button
   * 
   */
   @Prop() type: 'btn-primary' | 'btn-secondary' | 'btn-tertiary'  = 'btn-primary';
  /**
   * Icon Button
   * 
   */
   @Prop() icon: boolean = false;
   
  /**
   * Texto Button
   * 
   */
   @Prop() label: string = 'button';
  /**
   * Size Button
   * 
   */
   @Prop() size: 'btn-lg' | 'btn-md' | 'btn-sm' = 'btn-lg';
  /**
   * Disabled
   * 
   */
   @Prop() disabled: boolean = false;

  render() {
    if (this.icon) {
      return ( 
        <button class={'btn '+this.size + ' ' + this.type} disabled={this.disabled}>
          {this.label}
          {this.type == 'btn-primary'
            ? <icon-component icon="home" color="white" ></icon-component>
            : <icon-component icon="home" ></icon-component>
          }
          
        </button>
      )
    } else {
      return ( 
        <button class={'btn '+this.size + ' ' + this.type} disabled={this.disabled}>
          {this.label}
        </button>
       )
    }
  }
}
import { Component, Host, h } from '@stencil/core';

@Component({
  tag: 'tooltips-atoms',
  styleUrl: 'tooltips-atoms.css',
  shadow: true,
})
export class TooltipsAtoms {

  render() {
    return (
      <Host>
        <slot></slot>
      </Host>
    );
  }

}

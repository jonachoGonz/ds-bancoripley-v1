import { newSpecPage } from '@stencil/core/testing';
import { TooltipsAtoms } from '../tooltips-atoms';

describe('tooltips-atoms', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [TooltipsAtoms],
      html: `<tooltips-atoms></tooltips-atoms>`,
    });
    expect(page.root).toEqualHtml(`
      <tooltips-atoms>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </tooltips-atoms>
    `);
  });
});

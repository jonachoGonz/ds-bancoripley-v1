import { newE2EPage } from '@stencil/core/testing';

describe('tooltips-atoms', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<tooltips-atoms></tooltips-atoms>');

    const element = await page.find('tooltips-atoms');
    expect(element).toHaveClass('hydrated');
  });
});

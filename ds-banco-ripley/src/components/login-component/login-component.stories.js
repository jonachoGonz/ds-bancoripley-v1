import React from 'react';
//Readme
import readme from './readme.md';
//Resources Component
import { LoginComponent }  from '../../../dist/collection/components/login-component/login-component';
import cssCode              from '!!raw-loader!../../../dist/collection/components/login-component/login-component.css';
//Utils Functions
import { createControls, createPreviewsCode } from '../../utils/utils'
const controls    = createControls(LoginComponent.properties);
const previewData = createPreviewsCode({css: cssCode});

export default {
  title: 'Design System/Component/Login',
  component: LoginComponent,
  argTypes: controls,
  parameters: {
    notes: readme,
    preview: previewData
  }
};

const defaultArgs = {
  disabled: false,
};

const Template = args => {
  return <login-component {...args}></login-component>;
};

export const Default = Template.bind({});
Default.args = { ...defaultArgs };
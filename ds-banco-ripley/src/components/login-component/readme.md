# login-component



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type     | Default    |
| -------- | --------- | ----------- | -------- | ---------- |
| `label`  | `label`   |             | `string` | `'titulo'` |


## Dependencies

### Depends on

- [input-atoms](../inputs-atoms)
- [button-component](../button-component)

### Graph
```mermaid
graph TD;
  login-component --> input-atoms
  login-component --> button-component
  input-atoms --> icon-component
  button-component --> icon-component
  style login-component fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

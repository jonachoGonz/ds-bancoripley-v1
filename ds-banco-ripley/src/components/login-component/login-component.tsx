import { Component,h, Prop } from '@stencil/core';

@Component({
  tag: 'login-component',
  styleUrl: 'login-component.scss',
  shadow: true,
})
export class LoginComponent {
  @Prop() label: string = 'titulo'; 

  render() {
    return (
      <div class="container">
        <div class="login-container">
          <h1>{this.label}</h1>
          <input-atoms />
          <button-component />
        </div>
      </div>
    );
  }

}

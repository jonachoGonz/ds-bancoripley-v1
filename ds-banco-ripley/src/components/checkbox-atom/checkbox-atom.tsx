import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'checkbox-atom',
  styleUrl: 'checkbox-atom.scss',
  shadow: true,
})
export class CheckboxAtom {
  /**
   * Checkbox label
   * 
   */
  @Prop() label: string = 'Label';
  /**
   * Checkbox state
   * 
   */
   @Prop() state: 'error'|'disabled'|'default' = 'default';
   
  /**
   * Checkbox helper
   * 
   */
   @Prop() helper: string = 'error';


  render() {
    return (
      <div class={'inputGroup inputGroup-checkbox inputGroup-'+this.state}>
        {this.state == 'disabled'
          ? <input type='checkbox' disabled/>
          : <input type='checkbox' />
        }
        <label>
          <span class={'input-checkbox_box'} aria-hidden="true"></span>
          <span class={'input-checkbox_content'}>{this.label}</span>
        </label>
        {this.state == 'error'
          ? <span class="inputGroup-helper">{this.helper}</span>
          : ''
        }
        
      </div>
    );
  }

}

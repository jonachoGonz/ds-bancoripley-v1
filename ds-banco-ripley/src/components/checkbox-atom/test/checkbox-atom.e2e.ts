import { newE2EPage } from '@stencil/core/testing';

describe('checkbox-atom', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<checkbox-atom></checkbox-atom>');

    const element = await page.find('checkbox-atom');
    expect(element).toHaveClass('hydrated');
  });
});

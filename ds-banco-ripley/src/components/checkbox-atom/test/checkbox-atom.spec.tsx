import { newSpecPage } from '@stencil/core/testing';
import { CheckboxAtom } from '../checkbox-atom';

describe('checkbox-atom', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [CheckboxAtom],
      html: `<checkbox-atom></checkbox-atom>`,
    });
    expect(page.root).toEqualHtml(`
      <checkbox-atom>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </checkbox-atom>
    `);
  });
});

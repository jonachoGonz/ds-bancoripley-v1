import React from 'react';
//Readme
import readme from './readme.md';
//Resources Component
import { CheckboxAtom }  from '../../../dist/collection/components/checkbox-atom/checkbox-atom';
import cssCode              from '!!raw-loader!../../../dist/collection/components/checkbox-atom/checkbox-atom.css';
//Utils Functions
import { createControls, createPreviewsCode } from '../../utils/utils'
const controls    = createControls(CheckboxAtom.properties);
const previewData = createPreviewsCode({css: cssCode});

export default {
  title: 'Design System/Atoms/Checkbox',
  component: CheckboxAtom,
  argTypes: controls,
  parameters: {
    notes: readme,
    preview: previewData
  }
};

const defaultArgs = {
  disabled: false,
};

const Template = args => {
  return <checkbox-atom {...args}></checkbox-atom>;
};

export const Default = Template.bind({});
Default.args = { ...defaultArgs };
# checkbox-atom



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description     | Type                                 | Default     |
| -------- | --------- | --------------- | ------------------------------------ | ----------- |
| `helper` | `helper`  | Checkbox helper | `string`                             | `'error'`   |
| `label`  | `label`   | Checkbox label  | `string`                             | `'Label'`   |
| `state`  | `state`   | Checkbox state  | `"default" \| "disabled" \| "error"` | `'default'` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

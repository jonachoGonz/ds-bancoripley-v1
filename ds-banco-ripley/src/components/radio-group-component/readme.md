# radio-group-component



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type                                 | Default     |
| -------- | --------- | ----------- | ------------------------------------ | ----------- |
| `helper` | `helper`  | Input label | `string`                             | `'Helper'`  |
| `label`  | `label`   | Input label | `string`                             | `'Label'`   |
| `state`  | `state`   | Input state | `"default" \| "disabled" \| "error"` | `'default'` |


## Dependencies

### Depends on

- [radio-button-atom](../radio-button-atom)

### Graph
```mermaid
graph TD;
  radio-group-component --> radio-button-atom
  style radio-group-component fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

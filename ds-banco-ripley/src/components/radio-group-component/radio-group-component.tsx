import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'radio-group-component',
  styleUrl: 'radio-group-component.scss',
  shadow: true,
})
export class RadioGroupComponent {
/**
  * Input label
  * 
  */
 @Prop() label: string = 'Label';
/**
  * Input label
  * 
  */
 @Prop() helper: string = 'Helper';
 /**
 * Input state
 * 
 */
 @Prop() state: 'error'|'disabled'|'default' = 'default';
 
 render() {
   return (
     <div class="formGroup">
       <label class="formGroup-label">{this.label}</label>
       <radio-button-atom/>
       <span class="formGroup-helper">{this.helper}</span>

     </div>
   );
 }

}



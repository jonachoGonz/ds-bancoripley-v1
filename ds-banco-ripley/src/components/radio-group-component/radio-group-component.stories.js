import React from 'react';
//Readme
import readme from './readme.md';
//Resources Component
import { RadioGroupComponent }  from '../../../dist/collection/components/radio-group-component/radio-group-component';
import cssCode              from '!!raw-loader!../../../dist/collection/components/radio-group-component/radio-group-component.css';
//Utils Functions
import { createControls, createPreviewsCode } from '../../utils/utils'
const controls    = createControls(RadioGroupComponent.properties);
const previewData = createPreviewsCode({css: cssCode});

export default {
  title: 'Design System/Atoms/Radio Group',
  component: RadioGroupComponent,
  argTypes: controls,
  parameters: {
    notes: readme,
    preview: previewData
  }
};

const defaultArgs = {
  disabled: false,
};

const Template = args => {
  return <radio-group-component {...args}></radio-group-component>;
};

export const Default = Template.bind({});
Default.args = { ...defaultArgs };
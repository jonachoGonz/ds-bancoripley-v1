import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'radio-button-atom',
  styleUrl: 'radio-button-atom.scss',
  shadow: true,
})
export class RadioButtonAtom {
  /**
  * Input label
  * 
  */
  @Prop() label: string = 'Label';
  /**
  * Input state
  * 
  */
  @Prop() state: 'error'|'disabled'|'default' = 'default';
  /**
  * Id radio button
  * 
  */
  @Prop() value: string = 'radio-1';
  
  render() {
    return (
      <div class={'inputGroup inputGroup-'+this.state}>
        <div class='inputGroup-radioButton'>
          {this.state == 'disabled'
            ? <input type="radio" id={this.value} name="selector" disabled/>
            : <input type="radio" id={this.value} name="selector" />
          }
          <div class="inputGroup-check">
            <div class="inner-circle"></div>
            {/*todo-focus <div class="ripple"></div> */}
          </div>
        </div>
        <label htmlFor={this.value} class="inputGroup-label">{this.label}</label>
      </div>
    );
  }

}



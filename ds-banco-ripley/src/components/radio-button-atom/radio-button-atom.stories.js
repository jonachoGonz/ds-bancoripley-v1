import React from 'react';
//Readme
import readme from './readme.md';
//Resources Component
import { RadioButtonAtom }  from '../../../dist/collection/components/radio-button-atom/radio-button-atom';
import cssCode              from '!!raw-loader!../../../dist/collection/components/radio-button-atom/radio-button-atom.css';
//Utils Functions
import { createControls, createPreviewsCode } from '../../utils/utils'
const controls    = createControls(RadioButtonAtom.properties);
const previewData = createPreviewsCode({css: cssCode});

export default {
  title: 'Design System/Atoms/Radio',
  component: RadioButtonAtom,
  argTypes: controls,
  parameters: {
    notes: readme,
    preview: previewData
  }
};

const defaultArgs = {
  disabled: false,
};

const Template = args => {
  return <radio-button-atom {...args}></radio-button-atom>;
};

export const Default = Template.bind({});
Default.args = { ...defaultArgs };
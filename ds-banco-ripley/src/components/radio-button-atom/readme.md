# radio-button-atom



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description     | Type                                 | Default     |
| -------- | --------- | --------------- | ------------------------------------ | ----------- |
| `label`  | `label`   | Input label     | `string`                             | `'Label'`   |
| `state`  | `state`   | Input state     | `"default" \| "disabled" \| "error"` | `'default'` |
| `value`  | `value`   | Id radio button | `string`                             | `'radio-1'` |


## Dependencies

### Used by

 - [radio-group-component](../radio-group-component)

### Graph
```mermaid
graph TD;
  radio-group-component --> radio-button-atom
  style radio-button-atom fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

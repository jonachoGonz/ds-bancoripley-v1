import { newE2EPage } from '@stencil/core/testing';

describe('radio-button-atom', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<radio-button-atom></radio-button-atom>');

    const element = await page.find('radio-button-atom');
    expect(element).toHaveClass('hydrated');
  });
});

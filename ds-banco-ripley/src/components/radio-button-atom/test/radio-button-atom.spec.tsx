import { newSpecPage } from '@stencil/core/testing';
import { RadioButtonAtom } from '../radio-button-atom';

describe('radio-button-atom', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [RadioButtonAtom],
      html: `<radio-button-atom></radio-button-atom>`,
    });
    expect(page.root).toEqualHtml(`
      <radio-button-atom>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </radio-button-atom>
    `);
  });
});


//
// StyleDictionarySize.m
//

// Do not edit directly
// Generated on Fri, 24 Sep 2021 17:43:59 GMT


#import "StyleDictionarySize.h"


float const SizeFontSmall = 12.00f;
float const SizeFontMedium = 16.00f;
float const SizeFontLarge = 32.00f;
float const SizeFontBase = 16.00f;


// StyleDictionarySize.h
//

// Do not edit directly
// Generated on Fri, 24 Sep 2021 17:43:59 GMT


#import <Foundation/Foundation.h>


extern float const SizeFontSmall;
extern float const SizeFontMedium;
extern float const SizeFontLarge;
extern float const SizeFontBase;

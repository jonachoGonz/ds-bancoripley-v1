
//
// StyleDictionarySize.swift
//

// Do not edit directly
// Generated on Fri, 24 Sep 2021 17:43:59 GMT


import UIKit

public enum StyleDictionarySize {
    public static let fontBase = CGFloat(16.00) /* the base size of the font */
    public static let fontLarge = CGFloat(32.00) /* the large size of the font */
    public static let fontMedium = CGFloat(16.00) /* the medium size of the font */
    public static let fontSmall = CGFloat(12.00) /* the small size of the font */
}



// Do not edit directly
// Generated on Fri, 24 Sep 2021 17:43:59 GMT



package StyleDictionarySize;


import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.*

object StyleDictionarySize {
  /** the base size of the font */
  val sizeFontBase = 16.00.sp
  /** the large size of the font */
  val sizeFontLarge = 32.00.sp
  /** the medium size of the font */
  val sizeFontMedium = 16.00.sp
  /** the small size of the font */
  val sizeFontSmall = 12.00.sp
}

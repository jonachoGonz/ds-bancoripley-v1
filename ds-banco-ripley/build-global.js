

//  personal
  
const StyleDictionary = require('style-dictionary');
const tokens = require("./tokens/index.js");
const tokensBrands = require("./tokens/brands/index.js");
const tokensCore = require("./tokens/core/index.js");
const tokenSRCbrand = require("./tokens/core/index.js");

const brands = [`sitio-publico`, `pwa`,`cms`,`seguros`,`chek`];
brands.forEach(brand => {
  StyleDictionary.extend({
    // include: [`tokens/core/**/*.json`],
    source: [
        // `tokens/core/**/*.json`
        `src/tokens/global-tokens/token.json`
        // `tokens/core/**/*.json`,
    ],
    platforms: {
        'scss/category': {
          transformGroup: 'scss',
          
          // buildPath: 'tokens-build/ads/core/',
          buildPath: 'src/global/tokens/',
          prefix: "brad-global",
          transforms: ['attribute/cti','name/cti/kebab','size/px','color/hex'],
          "files": [{
            destination: `global/global-token.scss`,
            format: "scss/variables",
          // files: tokenSRCbrand.map((tokenCategory) => ({
          //   destination: `brand/${tokenCategory}/ads-brand-${tokenCategory}.scss`,
          //   format: "scss/variables",
          //   filter: {
          //     attributes: {
          //       category: tokenCategory,
          //     },
          //   },
          }]
          // files: [{
          //   destination: `ads/${brand}/ads-color-${brand}.scss`,
          //   format: 'scss/variables'
          // }]
        },
        // 'web': {
        //   transformGroup: 'scss',
        //   buildPath: 'tokens-build/',
        //   files: tokensBrands.map((tokenCategory) => ({
        //     destination: `ads/brands/${tokenCategory}/ads-${tokenCategory}-${brand}.scss`,
        //     format: "scss/variables",
        //     filter: {
        //       attributes: {
        //         category: tokenCategory,
        //       },
        //     },
        //   })),
        // }
        // ...
      }
    // ...
  }).buildAllPlatforms();
});
StyleDictionary.registerTransform({
  name: 'fontSizes/px',
  type: 'value',
  matcher: function(token) {
    return token.attributes.category === 'fontSizes';
  },
  transformer: function(token) {
    // Note the use of prop.original.value,
    // before any transforms are performed, the build system
    // clones the original token to the 'original' attribute.
  }
});
console.log('\n==============================================');
console.log('\nBuild completed!');
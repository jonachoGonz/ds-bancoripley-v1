

//  personal
  
const StyleDictionary = require('style-dictionary');
const tokens = require("./tokens/index.js");
const tokensBrands = require("./tokens/brands/index.js");
const tokensCore = require("./tokens/core/index.js");
const tokenSRCbrand = require("./tokens/core/index.js");

const brands = [`sitio-publico`, `pwa`,`cms`,`seguros`,`chek`];
brands.forEach(brand => {
  StyleDictionary.extend({
    // include: [`tokens/core/**/*.json`],
    source: [
        // `tokens/core/**/*.json`
        `src/tokens/brand-tokens/sp/token.json`
        // `tokens/core/**/*.json`,
    ],
    platforms: {
        'scss/category': {
          transformGroup: 'scss',
          
          // buildPath: 'tokens-build/ads/core/',
          buildPath: 'src/global/tokens/',
          prefix: "brad-",
          transforms: ['attribute/cti','name/cti/kebab','size/px','color/hex'],
          "files": [{
            destination: `brand/sp-token.scss`,
            format: "scss/variables",
          // files: tokenSRCbrand.map((tokenCategory) => ({
          //   destination: `brand/${tokenCategory}/ads-brand-${tokenCategory}.scss`,
          //   format: "scss/variables",
          //   filter: {
          //     attributes: {
          //       category: tokenCategory,
          //     },
          //   },
          }]
          // files: [{
          //   destination: `ads/${brand}/ads-color-${brand}.scss`,
          //   format: 'scss/variables'
          // }]
        },
        // 'web': {
        //   transformGroup: 'scss',
        //   buildPath: 'tokens-build/',
        //   files: tokensBrands.map((tokenCategory) => ({
        //     destination: `ads/brands/${tokenCategory}/ads-${tokenCategory}-${brand}.scss`,
        //     format: "scss/variables",
        //     filter: {
        //       attributes: {
        //         category: tokenCategory,
        //       },
        //     },
        //   })),
        // }
        // ...
      }
    // ...
  }).buildAllPlatforms();
});

console.log('\n==============================================');
console.log('\nBuild completed!');